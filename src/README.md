T3 Inteligencia Computacional
Gabriel Andrés Azócar Cárcamo

Modo de uso.
- Código de la generación de base de datos
Para dividir la base de datos se debe tener la base de datos de digitos
en la misma carpeta, con el nombre digits.csv. Luego basta ejecutar:
  python generate_db.py
Que generará dos archivos: uno con la base de datos de entrenamiento y otra con la de prueba.

- Código de la parte 3 de la tarea:
Para correrlo basta ejecutar:
  python parte_3.py
Esto guardará en una imagen y mostrará la matriz de confusión de la red entrenada.

- Código de la parte 4a de la tarea:
Para correrlo basta ejecutar:
  python parte_4a.py
Entrega todas las matrices de confusion de cada configuracion.

- Código de la parte 4b de la tarea:
Para correrlo basta ejecutar:
  python parte_4b.py
Entrega todos las metricas de la comparación.

