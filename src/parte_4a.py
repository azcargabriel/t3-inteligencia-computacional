# -*- coding: utf-8 -*-
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

from sklearn.neural_network import MLPClassifier
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score
from sklearn.metrics import confusion_matrix

def show_confusion_matrix(m, name):
    fig, ax = plt.subplots()    
    ax.matshow(m, cmap=plt.cm.Wistia)
    
    for i in xrange(10):
        for j in xrange(10):
            c = m[j,i]
            ax.text(i, j, str(c), va='center', ha='center')
 
    plt.savefig(str(name)+'.png', format='png', dpi=1000)

#Datos
f = pd.read_csv("trainingDB.csv", header=None)
X = f.values[:, 0:64]
y = f.values[:, 64]

#Se particiona la base de datos de entrenamiento y creamos el clasificador
X, X_val, y, y_val = train_test_split(X, 
                                      np.ravel(y), 
                                      random_state=np.random, 
                                      test_size=0.25)

j = 0
n = [1, 10, 15, 26, 37, 45, 55, 65, 80, 100, 200]
accs = []
while(j < len(n)):
    clf = MLPClassifier(activation='logistic', 
                        hidden_layer_sizes=(n[j]), 
                        early_stopping = True)
    clf.fit(X, np.ravel(y))
    
    #Clasificamos todos los ejemplos de X_val
    y_pred = np.array([])
    i = 0
    while i < len(X_val):
        y_pred = np.append(y_pred, clf.predict(X_val[i].reshape(1, -1)))
        i = i + 1
        
    #Medidas de rendimiento
    c_matrix = confusion_matrix(y_val, y_pred)
    acc = accuracy_score(y_val, y_pred)
    accs = np.append(accs, acc)
    
    print 'Red con ' + str(n[j]) +' neuronas en la capa oculta'
    show_confusion_matrix(c_matrix, n[j])
    j = j + 1
    
print accs