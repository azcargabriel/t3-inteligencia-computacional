# -*- coding: utf-8 -*-

import pandas as pd
from sklearn.utils import shuffle

f = pd.read_csv("digits.csv", header=None)
zero = shuffle(f.loc[f[64] == 0])
one = shuffle(f.loc[f[64] == 1])
two = shuffle(f.loc[f[64] == 2])
three = shuffle(f.loc[f[64] == 3])
four = shuffle(f.loc[f[64] == 4])
five = shuffle(f.loc[f[64] == 5])
six = shuffle(f.loc[f[64] == 6])
seven = shuffle(f.loc[f[64] == 7])
eight = shuffle(f.loc[f[64] == 8])
nine = shuffle(f.loc[f[64] == 9])

#DDBB -> 0 1 2 3 4 5 6 7 8 9
#testDB -> 111 117 111 116 114 111 111 116 114 112
#trainingDB -> 443 454 446 456 454 447 447 450 440 450

testDB = pd.concat([zero.iloc[0:111, :], one.iloc[0:117, :]])
testDB = pd.concat([testDB, two.iloc[0:111, :]])
testDB = pd.concat([testDB, three.iloc[0:116, :]])
testDB = pd.concat([testDB, four.iloc[0:114, :]])
testDB = pd.concat([testDB, five.iloc[0:111, :]])
testDB = pd.concat([testDB, six.iloc[0:111, :]])
testDB = pd.concat([testDB, seven.iloc[0:116, :]])
testDB = pd.concat([testDB, eight.iloc[0:114, :]])
testDB = pd.concat([testDB, nine.iloc[0:112, :]])

trainingDB = pd.concat([zero.iloc[111:, :], one.iloc[117:, :]])
trainingDB = pd.concat([trainingDB, two.iloc[111:, :]])
trainingDB = pd.concat([trainingDB, three.iloc[116:, :]])
trainingDB = pd.concat([trainingDB, four.iloc[114:, :]])
trainingDB = pd.concat([trainingDB, five.iloc[111:, :]])
trainingDB = pd.concat([trainingDB, six.iloc[111:, :]])
trainingDB = pd.concat([trainingDB, seven.iloc[116:, :]])
trainingDB = pd.concat([trainingDB, eight.iloc[114:, :]])
trainingDB = pd.concat([trainingDB, nine.iloc[112:, :]])

testDB.to_csv("testDB.csv", header=None, index=False)
trainingDB.to_csv("trainingDB.csv", header=None, index=False)

print 'done'