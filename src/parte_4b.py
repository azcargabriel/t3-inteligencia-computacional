# -*- coding: utf-8 -*-

import numpy as np
import pandas as pd
from datetime import datetime

from sklearn.neural_network import MLPClassifier
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score
from sklearn.metrics import confusion_matrix
        
#Datos
f = pd.read_csv("testDB.csv", header=None)
X = f.values[:, 0:64]
y = f.values[:, 64]

#Se particiona la base de datos de entrenamiento y creamos el clasificador
X, X_val, y, y_val = train_test_split(X, 
                                      np.ravel(y), 
                                      random_state=np.random, 
                                      test_size=0.25)

#Sumas de las diagonales
trace_sig = []
trace_relu = []

#Accuracy
accs_sig = []
accs_relu = []

#Tiempos de entrenamiento
train_time_sig = []
train_time_relu = [] 
startTime = datetime.now()

j = 0
while(j < 3):
    #Usando sigmoidal
    startTime = datetime.now()
    clf_sig = MLPClassifier(activation='logistic', 
                        hidden_layer_sizes=(45), 
                        early_stopping = True)
    clf_sig.fit(X, np.ravel(y))
    train_time_sig = np.append(train_time_sig, (datetime.now() - startTime).total_seconds())
    
    #Clasificamos todos los ejemplos de X_val
    y_pred_sig = np.array([])
    i = 0
    while i < len(X_val):
        y_pred_sig = np.append(y_pred_sig, clf_sig.predict(X_val[i].reshape(1, -1)))
        i = i + 1
        
    #Medidas de rendimiento sigmoidal
    c_matrix = confusion_matrix(y_val, y_pred_sig)
    trace_sig = np.append(trace_sig, np.trace(c_matrix))
    acc = accuracy_score(y_val, y_pred_sig)
    accs_sig = np.append(accs_sig, acc)
    
    
    #Usando relu
    startTime = datetime.now()
    clf_relu = MLPClassifier(activation='relu', 
                        hidden_layer_sizes=(45), 
                        early_stopping = True)
    clf_relu.fit(X, np.ravel(y))
    train_time_relu = np.append(train_time_relu, (datetime.now() - startTime).total_seconds())

    #Clasificamos todos los ejemplos de X_val
    y_pred_relu = np.array([])
    i = 0
    while i < len(X_val):
        y_pred_relu = np.append(y_pred_relu, clf_relu.predict(X_val[i].reshape(1, -1)))
        i = i + 1
        
    #Medidas de rendimiento sigmoidal
    c_matrix = confusion_matrix(y_val, y_pred_relu)
    trace_relu = np.append(trace_relu, np.trace(c_matrix))
    acc = accuracy_score(y_val, y_pred_relu)
    accs_relu = np.append(accs_relu, acc)

    j = j + 1

#Desviaciones standard
std_dev_sig = np.std(trace_sig)
std_dev_relu = np.std(trace_relu)

print 'Tiempo promedio de entrenamiento con funcion sigmoidal: ' + str(np.mean(train_time_sig))
print 'Promedio de la diagonal con sigmoidal: ' + str(np.mean(trace_sig))
print 'Desviacion standard del promedio de red con sigmoidal: ' + str(std_dev_sig) 
print 'Accuracy promedio: ' + str(np.mean(accs_sig)) + '\n'
print 'Tiempo promedio de entrenamiento con funcion relu: ' + str(np.mean(train_time_relu))
print 'Promedio de la diagonal con relu: ' + str(np.mean(trace_relu))
print 'Desviacion standard del promedio red con relu: ' + str(std_dev_relu)
print 'Accuracy promedio: ' + str(np.mean(accs_relu))
